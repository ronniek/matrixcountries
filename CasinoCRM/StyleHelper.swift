//import Foundation
//import UIKit
//
//public class StyleHelper {
//    private static var label = UILabel()
//    
//    static func styleNavigation(_ viewController: UIViewController) {
//        viewController.navigationController?.navigationBar.barTintColor = navBGColor
//        viewController.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: label.font.fontName, size: navTitleSize)!, NSAttributedString.Key.foregroundColor: navTitleColor]
//        viewController.navigationController?.navigationBar.tintColor = navButtonColor
//    }
//    static func styleNavigationBar(_ navigationBar: UINavigationBar) {
//        navigationBar.barTintColor = navBGColor
//        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: label.font.fontName, size: navTitleSize)!, NSAttributedString.Key.foregroundColor: navTitleColor]
//        navigationBar.isTranslucent = false
//        navigationBar.tintColor = navButtonColor
//    }
//    static func styleBarButtonItem(_ barButtonItem: UIBarButtonItem) {
//        barButtonItem.tintColor = navButtonColor
//    }
//    static func styleNavigationItem(_ navigationItem: UINavigationItem) {
//        navigationItem.backBarButtonItem?.tintColor = navButtonColor
//    }
//    
//    static func styleBG(_ view: UIView) {
//        view.backgroundColor = bgColor
//    }
//    
//    static func styleH1(_ label: UILabel) {
//        label.textColor = h1Color
//        label.font = UIFont(name: "Chalkboard SE", size: h1Size)
//    }
//    static func styleH2(_ label: UILabel) {
//        label.textColor = h2Color
//        label.font = label.font.withSize(h2Size)
//    }
//    static func styleH3(_ label: UILabel) {
//        label.textColor = regularColor
//        label.font = label.font.withSize(regularSize)
//    }
//    static func styleLabel(_ label: UILabel) {
//        label.textColor = labelColor
//        label.font = label.font.withSize(regularSize)
//    }
//    static func styleTextField(_ textField: UITextField) {
//        textField.textColor = textFieldValueColor
//        textField.backgroundColor = textFieldBGColor
//        textField.font = UIFont(name: label.font.fontName, size: regularSize)
//    }
//    static func styleTextView(_ textView: UITextView) {
//        textView.textColor = textFieldValueColor
//        textView.backgroundColor = textFieldBGColor
//        textView.font = UIFont(name: label.font.fontName, size: regularSize)
//    }
//    static func styleButton(_ button: UIButton) {
//        button.titleLabel?.tintColor = buttonColor
//        button.titleLabel?.font = UIFont(name: label.font.fontName, size: regularSize)
//   }
//    
//    static func styleKeyboard() {
//    }
//    static func styleActivityIndicator(_ activityIndicator: UIActivityIndicatorView) {
////        switch AppManager.instance.styleType {
////        case .dark:
////            activityIndicator.style = .white
////        case .light:
////            activityIndicator.style = .gray
////        case .color:
////            activityIndicator.style = .gray
////        }
//    }
//
//    static func styleTableView(_ tableView: UITableView) {
//        tableView.backgroundColor = tableViewBGColor
//        tableView.separatorColor = tableViewSeparatorColor
//    }
//    static func styleTableViewButton(_ buttonTVC: ButtonTVC) {
//        buttonTVC.backgroundColor = tableViewBGColor
//    }
//    static func styleTableViewLabel(_ labelTVC: LabelTVC) {
//        labelTVC.backgroundColor = tableViewBGColor
//        labelTVC.titleLabel.textColor = tableViewTitleColor
//        if (labelTVC.tableViewCellData as! LabelTVCData).valueColor == nil {
//            labelTVC.valueLabel.textColor = tableViewValueColor
//        } else {
//            labelTVC.valueLabel.textColor = (labelTVC.tableViewCellData as! LabelTVCData).valueColor
//        }
//        labelTVC.titleLabel.font = UIFont(name: label.font.fontName, size: tableViewLabel)
//        labelTVC.valueLabel.font = UIFont(name: label.font.fontName, size: regularSize)
//    }
//    static func styleTableViewDetails(_ detailsTVC: DetailsTVC) {
//        detailsTVC.backgroundColor = tableViewBGColor
//        detailsTVC.titleLabel.textColor = tableViewTitleColor
//        detailsTVC.valueLabel.textColor = tableViewValueColor
//        detailsTVC.titleLabel.font = UIFont(name: label.font.fontName, size: tableViewLabel)
//        detailsTVC.valueLabel.font = UIFont(name: label.font.fontName, size: regularSize)
//    }
//    static func styleTableViewCheckBox(_ checkBoxTVC: CheckBoxTVC) {
//        checkBoxTVC.backgroundColor = tableViewBGColor
//        checkBoxTVC.titleLabel.textColor = tableViewTitleColor
//        checkBoxTVC.titleLabel.font = UIFont(name: label.font.fontName, size: tableViewLabel)
//    }
//    static func styleTableViewLabelSegue(_ labelSegueTVC: LabelSegueTVC) {
//        labelSegueTVC.backgroundColor = tableViewBGColor
//        labelSegueTVC.titleLabel.textColor = tableViewTitleColor
//        labelSegueTVC.valueLabel.textColor = tableViewValueColor
//        labelSegueTVC.titleLabel.font = UIFont(name: label.font.fontName, size: tableViewLabel)
//        labelSegueTVC.valueLabel.font = UIFont(name: label.font.fontName, size: regularSize)
//    }
//    static func styleTableViewSwitch(_ switchTVC: SwitchTVC) {
//        switchTVC.backgroundColor = tableViewBGColor
//        switchTVC.titleLabel.textColor = tableViewTitleColor
//        switchTVC.onOffSwitch.onTintColor = tableViewSwitchColor
//        //switchTVC.onOffSwitch.thumbTintColor = navBGColor1
//        //switchTVC.onOffSwitch.tintColor = tableViewTextFieldBorderColor1
//        switchTVC.titleLabel.font = UIFont(name: label.font.fontName, size: tableViewLabel)
//    }
//    static func styleTableViewTextField(_ textFieldTVC: TextFieldTVC) {
//        textFieldTVC.backgroundColor = tableViewBGColor
//        textFieldTVC.titleLabel.textColor = tableViewTitleColor
//        textFieldTVC.textField.backgroundColor = tableViewTextFieldBGColor
//        textFieldTVC.textField.textColor = tableViewTextFieldTextColor
//        textFieldTVC.titleLabel.font = UIFont(name: label.font.fontName, size: tableViewLabel)
//        textFieldTVC.textField.font = UIFont(name: label.font.fontName, size: regularSize)
//    }
//    
//    public static let TVCHeight: CGFloat = 45
//    public static let MainMenuTVCHeight: CGFloat = 65
//    public static let MainMenuButtonSize: CGFloat = 30
//    
//    public static func ADTColor(_ adt:Double) -> UIColor {
//        if adt >= 500 {
//            return adt4Color
//        } else if adt >= 250 {
//            return adt3Color
//        } else if adt >= 100 {
//            return adt2Color
//        } else {
//            return adt1Color
//        }
//    }
//    
//    public static var yellowColor = StyleHelper.getColor("FCE205", "FCE205", "FCE205")
//    public static var greenColor = StyleHelper.getColor("4CBB17", "0b6623", "0b6623")
//    public static var blueColor = StyleHelper.getColor("89CFF0", "0E4D92", "0E4D92")
//    public static var orangeColor = StyleHelper.getColor("F9812A", "FF7417", "FF7417")
//    public static var redColor = StyleHelper.getColor("FF2400", "BF0A30", "BF0A30")
//    public static var blackColor = StyleHelper.getColor("0", "0", "0")
//
//    public static var navBGColor = StyleHelper.getColor("222222", "E4E4E4", "4267B1")
//    public static var navTitleColor = StyleHelper.getColor("FFFFFF", "0", "FEFEFE")
//    public static var navButtonColor = StyleHelper.getColor("007BFF", "007BFF", "FFFFFF")
//    
//    public static var bgColor = StyleHelper.getColor("2A2C2F", "F4F4F4", "E9EBEE")
//    public static var h1Color = StyleHelper.getColor("FFFFFF", "4267B1", "4267B1")
//    public static var h2Color = StyleHelper.getColor("AAAAAA", "444444", "0")
//    public static var regularColor = StyleHelper.getColor("FFFFFF", "181818", "1D2129")
//    public static var labelColor = StyleHelper.getColor("FFFFFF", "181818", "1D2129")
//    public static var labelGreenColor = greenColor
//    public static var labelRedColor = redColor
//    public static var textFieldValueColor = StyleHelper.getColor("FFFFFF", "181818", "1D2129")
//    public static var textFieldBorderColor = StyleHelper.getColor("131313", "9C9C9C", "DADEE4")
//    public static var textFieldBGColor = StyleHelper.getColor("43484E", "FFFFFF", "DADEE4")
//    public static var textFieldTextColor = StyleHelper.getColor("AAAAAA", "181818", "1D2129")
//    public static var buttonColor = StyleHelper.getColor("007BFF", "007BFF", "007BFF")
//    
//    public static var tableViewBGColor = StyleHelper.getColor("2A2C2F", "F4F4F4", "E9EBEE")
//    public static var tableViewHeaderBGColor = StyleHelper.getColor("222222", "E4E4E4", "4267B1")
//    public static var tableViewHeaderTitleColor = StyleHelper.getColor("FFFFFF", "0", "FEFEFE")
//    public static var tableViewFooterBGColor = StyleHelper.getColor("222222", "E4E4E4", "4267B1")
//    public static var tableViewFooterTitleColor = StyleHelper.getColor("FFFFFF", "0", "FEFEFE")
//    public static var tableViewSeparatorColor = StyleHelper.getColor("151617", "9F9F9F", "CCCCCC")
//    public static var tableViewTitleColor = StyleHelper.getColor("FFFFFF", "181818", "1D2129")
//    public static var tableViewValueColor = StyleHelper.getColor("FFFFFF", "181818", "1D2129")
//    public static var tableViewSwitchColor = StyleHelper.getColor("0398FF", "0398FF", "4267B1")
//    public static var tableViewTextFieldBorderColor = StyleHelper.getColor("131313", "9C9C9C", "DADEE4")
//    public static var tableViewTextFieldBGColor = StyleHelper.getColor("43484E", "FFFFFF", "DADEE4")
//    public static var tableViewTextFieldTextColor = StyleHelper.getColor("AAAAAA", "181818", "1D2129")
//    public static var tableViewButtonColor = StyleHelper.getColor("007BFF", "007BFF", "007BFF")
//
//    public static var popupTitleColor = StyleHelper.getColor("0", "444444", "0")
//    public static var popupMessageColor = StyleHelper.getColor("0", "181818", "1D2129")
//
//    public static var adt4Color = StyleHelper.getColor("FF0800", "BF0A30", "BF0A30")
//    public static var adt3Color = StyleHelper.getColor("FCE205", "FFD300", "FFD300")
//    public static var adt2Color = StyleHelper.getColor("4CBB17", "0b6623", "0b6623")
//    public static var adt1Color = StyleHelper.getColor("FFFFFF", "0", "0")
//
//    public static var navTitleSize = CGFloat(22)
//    public static var h1Size = CGFloat(35)
//    public static var h2Size = CGFloat(24)
//    public static var regularSize = CGFloat(18)
//    public static var tableViewLabel = CGFloat(16)
//
//    public static func getColor(_ dark: String, _ light: String, _ color: String) -> UIColor {
//        return UIColor.white
//    }
//}
