import UIKit
import UserNotifications
import SwiftDate

class AppDelegate: AppDelegateEx {
    // class
    open override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        _ = super.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //NotificationsHandler.instance.initForRemoteNotifications(self, application)
        
        //self.window = UIWindow(frame: UIScreen.main.bounds)
        //self.window?.backgroundColor = UIColor.white
        //self.window?.makeKeyAndVisible()
        //window?.rootViewController = MyViewController()

        return true
    }
}
