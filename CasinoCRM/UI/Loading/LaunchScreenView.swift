import SwiftGifOrigin

class LaunchScreenView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    
    init() {
        imageView.image = UIImage.gif(name: "IconAnimation")

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
