import UIKit

class CountriesVC : UITableViewController {
    // members
    var countries = Array<CountryWSData>()
    var selectedCountry: CountryWSData?
    var borderedCountries: Array<CountryWSData>?
    
    override func viewDidLoad() {
        let dataAdapter = CountryAdapter("name;nativeName;alpha3Code;area;borders")
        dataAdapter.addCompletedEventListener { _ in
            // use object mapper to convert the JSON to a type-safe & name-safe data object
            self.countries = dataAdapter.wsResult ?? Array<CountryWSData>()
            
            // use the data in the code
            self.navigationItem.title = "Countries (\(self.countries.count))"
            
            self.tableView.reloadData()
        }
        dataAdapter.addErrorEventListener { _ in
            PopupsHelper.presentMessageBox(self, "Error", "Failed to get the data from the server", PopupsHelper.createDismissAlertAction("OK"))
        }
        dataAdapter.execute()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CountriesVCToCountryVC" {
            let targetVC = segue.destination as! CountryVC
            
            borderedCountries = Array<CountryWSData>()
            for country in countries {
                if selectedCountry?.borders.contains(country.alpha3Code) ?? false {
                    borderedCountries?.append(country)
                }
            }

            targetVC.selectCountry = selectedCountry
            targetVC.countries = borderedCountries
        }
    }
    
    // table view
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCountry = countries[indexPath.row]
        self.performSegue(withIdentifier: "CountriesVCToCountryVC", sender: self)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountriesCell", for: indexPath) as! CountriesTableViewCell
        let country = countries[indexPath.row]
        cell.nameLabel.text = country.name
        cell.nativeNameLabel.text = country.nativeName
        cell.areaLabel.text = "Area: \(country.area)"

        return cell;
    }
    
    @IBAction func sortClick(_ sender: Any) {
        PopupsHelper.presentActionSheet(self, "Sort Options", "",
                PopupsHelper.createDefaultAlertAction("Name A -> Z", { _ in
                    self.countries = self.countries.sorted(by: { $0.nativeName < $1.nativeName })
                    self.tableView.reloadData()
                }), PopupsHelper.createDefaultAlertAction("Name Z -> A", { _ in
                    self.countries = self.countries.sorted(by: { $0.nativeName > $1.nativeName })
                    self.tableView.reloadData()
                }), PopupsHelper.createDefaultAlertAction("Area Small -> Big", { _ in
                    self.countries = self.countries.sorted(by: { $0.area < $1.area })
                    self.tableView.reloadData()
                }), PopupsHelper.createDefaultAlertAction("Area Big -> Small", { _ in
                    self.countries = self.countries.sorted(by: { $0.area > $1.area })
                    self.tableView.reloadData()
                }), PopupsHelper.createCancelAlertAction({ _ in
                }))
    }
}
