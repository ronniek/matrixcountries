import UIKit

class CountriesTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nativeNameLabel: UILabel!
    @IBOutlet var areaLabel: UILabel!
}
