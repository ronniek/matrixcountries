import UIKit

class SettingsVC: UIViewControllerEx {
    // members
    var tableViewEx: UITableViewEx?
    let sections = TVSCollection()
    
    // class
    override func viewDidLoad() {        
        if tableViewEx == nil {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            tableViewEx?.addSegueListener(segueFunction)
            view.addSubview(tableViewEx!)
            
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        tableViewEx?.setSections(getSections())
    }
    public override func toRootController() {
        dismissToRootController()
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        let section1 = TVSData("", 45, 120, " ")
        section1.rows.append(LabelSegueTVCData(-1, 0, 0, "About", "", StoryboardSegueConsts.SettingsToAbout, UIColor.black))
        section1.rows.append(LabelSegueTVCData(-1, 0, 1, "Themes", "", StoryboardSegueConsts.SettingsToTheme, UIColor.black))
        sections.sections.append(section1)

//        if AppManager.instance.application.Configs.SettingsCanChangeLanguage ?? false {
//            let section1 = TVSData("", Style.TVCHeight, 120, "")
//            section1.rows.append(LabelSegueTVCData(0, 0, "Language", "", UIColor.black, StoryboardSegueConsts.SettingsToLanguage))
//            sections.sections.append(section1)
//        }
        
        let section3 = TVSData("", 45, 120, "")
        section3.rows.append(LabelSegueTVCData(-1, 0, 1, "Debug", "", StoryboardSegueConsts.SettingsToDebug, UIColor.black))
        sections.sections.append(section3)

        return sections
    }
    
    // events
    public func segueFunction(_ titleLabelSegueTableViewCellData: LabelSegueTVCData) -> () {
        performSegue(withIdentifier: titleLabelSegueTableViewCellData.performSegue, sender: self)
    }
}
