import UIKit

class DebugLocalizationVC: UIViewControllerEx {
    // members
    var tableViewEx: UITableViewEx?
    let sections = TVSCollection()
    
    // class
    override func viewDidLoad() {
        if tableViewEx == nil {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(tableViewEx!)
            
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        tableViewEx?.setSections(getSections())
    }
    public override func toRootController() {
        dismissToRootController()
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        let section1 = TVSData("Date Time", 45 * 1.2, 120, " ")
        section1.rows.append(DetailsTVCData(-1, 0, 0, "Full DateTime", DateTimeHelper.dateFullFormat(Date())))
        section1.rows.append(DetailsTVCData(-1, 0, 1, "Long DateTime", DateTimeHelper.dateLongFormat(Date())))
        section1.rows.append(DetailsTVCData(-1, 0, 2, "Medium DateTime", DateTimeHelper.dateMediumFormat(Date())))
        section1.rows.append(DetailsTVCData(-1, 0, 3, "Short DateTime", DateTimeHelper.dateShortFormat(Date())))
        section1.rows.append(DetailsTVCData(-1, 0, 4, "UTC", DateTimeHelper.dateToUTC(Date())))
        sections.sections.append(section1)

        let section2 = TVSData("Number", 45 * 1.2, 120, "")
        section2.rows.append(DetailsTVCData(-1, 1, 0, "Currency", NumberHelper.numberCurrencyFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 1, "Accounting", NumberHelper.numberAccountingFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 2, "ISO", NumberHelper.numberISOFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 3, "Plural", NumberHelper.numberPluralFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 4, "Decimal", NumberHelper.numberDecimalFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 5, "None", NumberHelper.numberNoneFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 6, "Ordinal", NumberHelper.numberOrdinalFormat(Decimal(12345.67))))
        section2.rows.append(DetailsTVCData(-1, 1, 7, "Spell out", NumberHelper.numberSpellOutFormat(Decimal(12345.67))))
        sections.sections.append(section2)
        
        return sections
    }
}
