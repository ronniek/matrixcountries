import UIKit

class DebugDeviceVC: UIViewControllerEx {
    // members
    var tableViewEx: UITableViewEx?
    let sections = TVSCollection()
    
    // class
    override func viewDidLoad() {
        if tableViewEx == nil {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            tableViewEx?.addButtonClickedListener(buttonClickedFunction)
            view.addSubview(tableViewEx!)
            
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        tableViewEx?.setSections(getSections())
    }
    public override func toRootController() {
        dismissToRootController()
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        let section1 = TVSData("", 45, 120, "")
        section1.rows.append(LabelTVCData(-1, 0, 0, "Name", UIDevice.current.name))
        section1.rows.append(LabelTVCData(-1, 0, 1, "System name", UIDevice.current.systemName))
        section1.rows.append(LabelTVCData(-1, 0, 2, "System version", UIDevice.current.systemVersion))
        section1.rows.append(LabelTVCData(-1, 0, 3, "Localized model", UIDevice.current.localizedModel))
        section1.rows.append(LabelTVCData(-1, 0, 4, "Model", UIDevice.current.model))
        section1.rows.append(LabelTVCData(-1, 0, 5, "Orientation", String(describing: UIDevice.current.orientation.rawValue)))
        section1.rows.append(LabelTVCData(-1, 0, 6, "User interface idiom", String(describing: UIDevice.current.userInterfaceIdiom.rawValue)))
        section1.rows.append(ButtonTVCData(-1, 0, 7, "Beep", HAlignmentTypeEnum.Center, UIColor.black, CGFloat(18)))
        section1.rows.append(ButtonTVCData(-1, 0, 8, "Vibrate", HAlignmentTypeEnum.Center, UIColor.black, CGFloat(18)))
        sections.sections.append(section1)
        
        return sections
    }
    
    // events
    public func buttonClickedFunction(_ buttonTableViewCellData: ButtonTVCData) -> () {
        if buttonTableViewCellData.row == 7 {
            DeviceHelper.beep()
        } else if buttonTableViewCellData.row == 8 {
            DeviceHelper.vibrate()
        }
    }
}
