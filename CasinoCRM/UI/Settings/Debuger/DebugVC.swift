import UIKit

class DebugVC: UIViewControllerEx {
    // members
    var tableViewEx: UITableViewEx?
    let sections = TVSCollection()
    
    // class
    override func viewDidLoad() {
        if tableViewEx == nil {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            tableViewEx?.addSegueListener(segueFunction)
            view.addSubview(tableViewEx!)
            
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        tableViewEx?.setSections(getSections())
    }
    public override func toRootController() {
        dismissToRootController()
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        let section1 = TVSData("", 45, 120, "")
        section1.rows.append(LabelTVCData(-1, 0, 0, "App Version", BundleHelper.getVersionString()))
            section1.rows.append(LabelSegueTVCData(-1, 0, 1, "Device", DeviceHelper.getName(), StoryboardSegueConsts.DebugToDebugDevice, UIColor.black))
        section1.rows.append(LabelTVCData(-1, 0, 2, "Network", ApplicationHandler.instance.networkStatus.isReachable ? "Connected" : "Disconnected"))
            section1.rows.append(LabelSegueTVCData(-1, 0, 3, "Server", "", StoryboardSegueConsts.DebugToDebugCasinoCRMServer, UIColor.black))
            section1.rows.append(LabelSegueTVCData(-1, 0, 4, "Configurations", "", StoryboardSegueConsts.DebugToDebugConfigurations, UIColor.black))
            section1.rows.append(LabelSegueTVCData(-1, 0, 5, "Localization", "", StoryboardSegueConsts.DebugToDebugLocalization, UIColor.black))
        //section1.rows.append(LabelSegueTVCData("", 0, 6, "Strings", "", StoryboardSegueConsts.DebugToDebugStrings, UIColor.black))
            section1.rows.append(LabelSegueTVCData(-1, 0, 7, "Management", "", StoryboardSegueConsts.DebugToDebugManagement, UIColor.black))
            section1.rows.append(LabelSegueTVCData(-1, 0, 8, "Linea Pro", "", StoryboardSegueConsts.DebugToDebugLineaPro, UIColor.black))
            // todo: add permission
            section1.rows.append(LabelSegueTVCData(-1, 0, 9, "Barcode", "", StoryboardSegueConsts.DebugToDebugBarcode, UIColor.black))
            section1.rows.append(LabelSegueTVCData(-1, 0, 10, "Badge Printer", "", StoryboardSegueConsts.DebugToDebugBadgePrinter, UIColor.black))
        // todo - add permission
            section1.rows.append(LabelSegueTVCData(-1, 0, 11, "Cached Users", "", StoryboardSegueConsts.DebugToDebugCachedUsers, UIColor.black))
        sections.sections.append(section1)
        
        return sections
    }
    
    // events
    public func segueFunction(_ titleLabelSegueTableViewCellData: LabelSegueTVCData) -> () {
        performSegue(withIdentifier: titleLabelSegueTableViewCellData.performSegue, sender: self)
    }
    @IBAction func doneClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

