import UIKit

class CountryVC: UIViewControllerEx {
    // members
    var tableViewEx: UITableViewEx?
    var sections = TVSCollection()
    var selectCountry: CountryWSData?
    var countries: Array<CountryWSData>?
    
    // class
    override func viewDidLoad() {
        if tableViewEx == nil {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(tableViewEx!)
        
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        
        self.navigationItem.title = selectCountry?.name

        self.tableViewEx?.setSections(self.getSections())
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        sections = TVSCollection()

        if countries?.count ?? 0 == 0 {
            let section1 = TVSData("", 45, 130, " ")
            section1.rows.append(LabelTVCData(-1, 0, 0, "No border with countries", ""))
            sections.sections.append(section1)
        } else {
            let section1 = TVSData("", 45, 130, " ")
            var row = 0
            for item in countries! {
                section1.rows.append(LabelSegueTVCData(row, 0, row, item.name, item.nativeName, StoryboardSegueConsts.AddressesToAddress, UIColor.black))
                row = row + 1
            }
            sections.sections.append(section1)
        }
        
        return sections
    }
}
