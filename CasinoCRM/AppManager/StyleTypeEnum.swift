import Foundation

public enum StyleTypeEnum: String {
    case light = "1"
    case dark = "2"
    case color = "3"
}
