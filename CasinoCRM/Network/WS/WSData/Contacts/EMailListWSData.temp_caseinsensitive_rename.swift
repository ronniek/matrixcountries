import ObjectMapper

open class EMailListWSData: BaseWSData {
    // members
    public var list: Array<EMailWSData>?
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        list <- map["List"]
    }
    
    // class
    public required init?(map: Map){
        self.list = Array()
        super.init(map: map)
    }
}
