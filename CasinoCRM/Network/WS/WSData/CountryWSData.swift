import ObjectMapper

open class CountryWSData {
    // members
    public var name: String
    public var alpha3Code: String
    public var area: Double
    public var borders: Array<String>
    public var nativeName: String

    // class
    public init() {
        self.name = ""
        self.alpha3Code = ""
        self.area = 0
        self.borders = Array<String>()
        self.nativeName = ""
   }
}
