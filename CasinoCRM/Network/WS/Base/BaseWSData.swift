import ObjectMapper

open class BaseWSData: BaseMappableDispatcher {
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
    }
    
    // class
    public override init() {
        super.init()
    }
    public required init?(map: Map) {
        super.init(map: map)
    }
}
