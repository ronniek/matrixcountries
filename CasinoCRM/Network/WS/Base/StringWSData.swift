import ObjectMapper

open class StringWSData: BaseWSData {
    // members
    public var Value: String
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        Value <- map["Value"]
    }
    
    // class
    public init(_ string: String) {
        self.Value = string
        super.init()
    }
    public required init?(map: Map) {
        self.Value = ""
        super.init(map: map)
    }
}
