import ObjectMapper

open class StringListWSData: BaseWSData {
    // members
    public var List: Array<StringWSData>
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        List <- map["List"]
    }
    
    // class
    public override init() {
        self.List = Array<StringWSData>()
        super.init()
    }
    public init(_ list: Array<StringWSData>) {
        self.List = list
        super.init()
    }
    public required init?(map: Map) {
        self.List = Array()
        super.init(map: map)
    }
}
