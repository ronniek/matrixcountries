import ObjectMapper

open class BoolWSData: BaseWSData {
    // members
    public var Value: Bool
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        Value <- map["Value"]
    }
    
    // class
    public override init() {
        self.Value = false
        super.init()
    }
    public init(_ bool: Bool) {
        self.Value = bool
        super.init()
    }
    public required init?(map: Map) {
        self.Value = false
        super.init(map: map)
    }
}
