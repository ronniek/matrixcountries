import UIKit;

public class CountryAdapter : BaseWSAdapter<Array<CountryWSData>> {
    // members
    override open var wsPath: String {
        return "all"
    }
    
    // class
    init(_ fields: String) {
        super.init()
        
        addParams("fields", fields)
    }
    
    // methods
    public override func extractData(_ items: [[String:Any]]) {
        wsResult = Array<CountryWSData>()
        
        for item in items {
            let country = CountryWSData()
            country.name = item["name"] as! String
            country.alpha3Code = item["alpha3Code"] as! String
            country.area = (item["area"] as? Double) ?? 0
            country.nativeName = item["nativeName"] as! String
            
            country.borders = Array<String>()
            for border in item["borders"] as! [String] {
                country.borders.append(border)
            }

            wsResult?.append(country)
        }
    }
}
