import Foundation

public enum ContentTypeEnum: String {
    case ApplicationJavascript = "application/javascript"
    case ApplicationJson = "application/json"
    case ApplicationForm = "application/x-www-form-urlencoded"
    case ApplicationPdf = "application/pdf"
    case ApplicationXml = "application/xml"
    case ApplicationZip = "application/zip"
    case AudioMpeg = "audio/mpeg"
    case AudioVorbis = "audio/vorbis"
    case MultipartFormData = "multipart/form-data"
    case TextCss = "text/css"
    case TextHtml = "text/html"
    case TextPlain = "text/plain"
    case ImagePng = "image/png"
    case ImageJpeg = "image/jpeg"
    case ImageGif = "image/gif"
}
