import Alamofire
import AlamofireObjectMapper

open class BaseWSAdapter<T> : EventDispatcher {
    // members
    public var isSuccess: Bool
    public var title: String
    public var text: String
    public var wsResult: T?
    public var json: String
    
    open var wsPath: String {
        return "override to set ws path"
    }
    
    open var httpMethod: HTTPMethod {
        return HTTPMethod.get
    }
    
    open var httpHeaders: HTTPHeaders {
        let httpHeaders = HTTPHeaders()
        //httpHeaders["Accept"] = ContentTypeEnum.ApplicationJson.rawValue
        return httpHeaders
    }
    
    open var httpParameters = [String: Any]()
    
    open var encoding: ParameterEncoding = URLEncoding.queryString
    
    open var random: Bool {
        return false
    }
    
    // class
    public override init() {
        isSuccess = false
        title = ""
        text = ""
        wsResult = nil
        json = ""
    }
    
    // methods
    public func addParams(_ key:String, _ value: String) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Int) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: UInt32) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Int64) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Double) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Date) {
        httpParameters[key] = DateTimeHelper.dateToYYYYMMDD_HHMMSS(value)
    }
    public func addParams(_ key:String, _ value: Bool) {
        if value {
            addParams(key, "true")
        } else {
            addParams(key, "false")
        }
    }
    
    // execute
    public func executeTest() {
        addParams("test", Consts.TEST_TOKEN)
        executeAdapter()
    }
    public func execute() {
        addParams("test", "")
        executeAdapter()
    }
    private func executeAdapter() {
        let url: URL = FileHelper.concat(URL(string: BundleHelper.getServerRootUrl())!, wsPath)!
        
        if random {
            addParams("random", arc4random_uniform(999999999))
        }

        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 50

        Alamofire
            .request(url, method: httpMethod, parameters: httpParameters, encoding: encoding, headers: httpHeaders)
            .responseJSON {
            response in

            switch response.result {
            case .success(let value):
                //to get JSON return value
                if let result = response.result.value {
                    let items = result as! [[String:Any]]
                    
                    self.extractData(items)
                    
                    self.postExecute(true)
                }

            case .failure(let error):
                print(error)
                self.postExecute(false)
            }
        }

    }
    public func postExecute(_ isSuccess: Bool) {
        if isSuccess {
            self.RaiseEvent(self.CompletedEvent, self)
        } else {
            if title == "" {
                title = "Error"
            }
            if text == "" {
                text = "Server error. Please contact the administrator"
            }
            self.RaiseEvent(self.ErrorEvent, self)
        }
    }
    
    public func extractData(_ items: [[String:Any]]) {
    }
    
    private func compareDicPrePost(_ check: String, _ dic1: [String: Any], _ dic2: [String: Any]) -> String {
        for (key, _) in dic1 {
            switch key {
            case "CompletedEvent":
                continue
            case "listeners":
                continue
            case "ErrorEvent":
                continue
            default:
                let o = dic2[key]
                if o == nil {
                    return "\(key) not found in \(check)"
                }
            }
        }
        
        return ""
    }
}
