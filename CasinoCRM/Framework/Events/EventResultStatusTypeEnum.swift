import Foundation

public enum EventResultStatusTypeEnum {
    case success
    case failed
}
