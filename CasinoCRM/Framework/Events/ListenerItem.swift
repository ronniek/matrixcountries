class ListenerItem {
    // members
    var type: String
    var function: (EventResult) -> ()
    
    // class
    init(_ type: String, _ function: @escaping (EventResult) -> ()) {
        self.type = type
        self.function = function
    }
}
