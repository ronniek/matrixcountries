import Foundation

public class EventSampleData: EventDispatcher {
    // members
    public var name: String = "" {
        didSet {
            RaiseEvents(EventResult("", self))
            RaiseEvent(EventResult(NameChangedEvent, self))
            RaiseEvent(EventResult(NameChangedEvent, self, "errorCode", "this is an error", "title for popup", "message for popup"))
        }
    }
    
    // event dispatcher
    private let NameChangedEvent = "NameChangedEvent"
    public func addNameChangedEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(NameChangedEvent, function)
    }
}
