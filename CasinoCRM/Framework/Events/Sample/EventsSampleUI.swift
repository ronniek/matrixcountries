import Foundation

public class EventsSampleUI {
    private var data = EventSampleData()
    
    public func DoSomething() {
        data.addNameChangedEventListener(somethingHappened)
        data.addNameChangedEventListener(somethingHappened)
        data.addNameChangedEventListener(somethingHappened)
        data.addNameChangedEventListener(somethingHappened)

        print(data.name)
        data.name = "Ronnie"
        print(data.name)
    }
    
    public func somethingHappened(_ eventResult: EventResult) -> () {
        let eventSampleData = eventResult.sender as! EventSampleData
        print("SomethingHappened(\(String(describing: eventResult.sender)), \(eventResult.type), \(eventSampleData.name))")
    }
}
