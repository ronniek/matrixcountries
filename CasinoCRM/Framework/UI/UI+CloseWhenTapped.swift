import Foundation
import UIKit

extension UIView {
    public func closeWhenTapped() {
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIView.dismissKeyboard))
        tapGestureRecognizer.cancelsTouchesInView = false
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func dismissKeyboard() {
        self.endEditing(true)
    }
}

extension UIViewController {
    @objc public func closeWhenTapped() {
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UITableViewController {
    public override func closeWhenTapped() {
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UITableViewController.dismissKeyboard))
        tapGestureRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    public override func dismissKeyboard() {
        tableView.endEditing(true)
    }
}
