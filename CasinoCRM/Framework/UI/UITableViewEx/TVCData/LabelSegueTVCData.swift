import UIKit;

class LabelSegueTVCData: BaseSegueTVCData {
    // members
    private var label: String
    public var valueColor: UIColor

    override var value: String {
        get {
            return label
        }
        set(value) {
            label = value
        }
    }
    
    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ label: String, _ performSegue: String, _ valueColor: UIColor) {
        self.label = label
        self.valueColor = valueColor
        super.init(id, section, row, TVCTypeEnum.TitleLabelSegue, title, performSegue)
    }
}
