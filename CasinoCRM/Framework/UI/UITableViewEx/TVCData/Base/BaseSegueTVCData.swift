class BaseSegueTVCData: BaseTVCData {
    // members
    public var performSegue: String

    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ tableViewCellType: TVCTypeEnum, _ title: String, _ performSegue: String) {
        self.performSegue = performSegue
        super.init(id, section, row, tableViewCellType, title)
    }
}
