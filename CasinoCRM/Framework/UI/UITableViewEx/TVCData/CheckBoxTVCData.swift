class CheckBoxTVCData: BaseTVCData {
    // members
    public var checkBox: Bool
    
    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ checkBox: Bool) {
        self.checkBox = checkBox
        super.init(id, section, row, TVCTypeEnum.TitleCheckBox, title)
    }
}
