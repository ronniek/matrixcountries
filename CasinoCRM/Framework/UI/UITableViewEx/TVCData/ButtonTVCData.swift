import UIKit;

class ButtonTVCData: BaseTVCData {
    // members
    var hAlignmentType: HAlignmentTypeEnum
    var buttonColor: UIColor
    var buttonFontSize: CGFloat

    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ hAlignmentType: HAlignmentTypeEnum, _ buttonColor: UIColor, _ buttonFontSize: CGFloat) {
        self.hAlignmentType = hAlignmentType
        self.buttonColor = buttonColor
        self.buttonFontSize = buttonFontSize
        super.init(id, section, row, TVCTypeEnum.Button, title)
    }
}
