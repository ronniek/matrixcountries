import UIKit

class SampleVC: UIViewController {
    // members
    var tableViewEx: UITableViewEx?
    let sections = TVSCollection()
    
    // class
    override func viewDidLoad() {
        if (tableViewEx == nil) {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            tableViewEx?.addButtonClickedListener(buttonClickedFunction)
            tableViewEx?.addSegueListener(segueFunction)
            tableViewEx?.addSwitchChangedListener(switchChangedFunction)
            tableViewEx?.addTextFieldChangedListener(textFieldChangedFunction)
            view.addSubview(tableViewEx!)
        
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        tableViewEx?.setSections(getSections())
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        let section1 = TVSData("header1", 45, 120, "Clicking on the [-] marks an episode or a season of aired episodes as watched (or downloaded) and removes it from the list.")
        section1.rows.append(LabelTVCData(-1, 0, 0, "title", "label"))
        section1.rows.append(TextFieldTVCData(-1, 0, 1, "textField1", section1.titleWidth, "text", true, UIKeyboardType.alphabet))
        sections.sections.append(section1)

        let section2 = TVSData("header2", 45, 120, "footer2")
        section2.rows.append(ButtonTVCData(-1, 1, 0, "button blue", HAlignmentTypeEnum.Left, UIColor.black, 18))
        section2.rows.append(ButtonTVCData(-1, 1, 1, "button red", HAlignmentTypeEnum.Center, UIColor.black, 18))

        section2.rows.append(CheckBoxTVCData(-1, 1, 2, "checkBox on", true))
        section2.rows.append(CheckBoxTVCData(-1, 1, 3, "checkBox on", true))
        section2.rows.append(CheckBoxTVCData(-1, 1, 4, "checkBox on", true))
        section2.rows.append(CheckBoxTVCData(-1, 1, 5, "checkBox off", false))

        section2.rows.append(LabelSegueTVCData(-1, 1, 6, "Title", "Label", "segue", UIColor.black))
        section2.rows.append(LabelSegueTVCData(-1, 1, 7, "Title Title Title Title Title", "Label", "segue", UIColor.black))
        section2.rows.append(LabelSegueTVCData(-1, 1, 8, "Title", "Label Label Label Label Label Label Label", "segue", UIColor.black))
        section2.rows.append(LabelSegueTVCData(-1, 1, 9, "Title Title Title Title Title", "Label Label Label Label Label Label Label", "segue", UIColor.black))

        section2.rows.append(LabelTVCData(-1, 1, 10, "Title", "label"))

        section2.rows.append(SwitchTVCData(-1, 1, 11, "checkBox on", true, true))
        section2.rows.append(SwitchTVCData(-1, 1, 12, "checkBox off", false, true))

        section2.rows.append(TextFieldTVCData(-1, 1, 13, "textField1", section2.titleWidth, "text", true, UIKeyboardType.asciiCapable))
        section2.rows.append(TextFieldTVCData(-1, 1, 14, "textField2", section2.titleWidth, "text", false, UIKeyboardType.emailAddress))
        section2.rows.append(TextFieldTVCData(-1, 1, 15, "textField3", section2.titleWidth, "", true, UIKeyboardType.decimalPad))
        section2.rows.append(TextFieldTVCData(-1, 1, 16, "textField4", section2.titleWidth, "", false, UIKeyboardType.default))
        sections.sections.append(section2)
        
        return sections
    }
    
    // events
    public func buttonClickedFunction(_ buttonTVCData: ButtonTVCData) -> () {
        print("SampleVC-buttonTVCData: {0}".format(buttonTVCData.title as AnyObject))
    }
    public func segueFunction(_ titleLabelSegueTVCData: LabelSegueTVCData) -> () {
        print("SampleVC-titleLabelSegueTVCData: {0}".format(titleLabelSegueTVCData.performSegue as AnyObject))
    }
    public func switchChangedFunction(_ titleSwitchTVCData: SwitchTVCData) -> () {
        print("SampleVC-titleSwitchTVCData: {0}".format(titleSwitchTVCData.isOn as AnyObject))
    }
    public func textFieldChangedFunction(_ titleSwitchTVCData: TextFieldTVCData) -> () {
        print("SampleVC-titleSwitchTVCData: {0}".format(titleSwitchTVCData.value as AnyObject))
    }

    @IBAction func itemClick(_ sender: Any) {
        for section in sections.sections {
            for row in section.rows {
                print("{0}".format(row.title as AnyObject))
            }
        }
    }
}
