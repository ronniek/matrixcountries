enum TVCTypeEnum {
    case Button
    case TitleCheckBox
    case TitleDetails
    case TitleLabel
    case TitleLabelSegue
    case TitleSwitch
    case TitleTextField
}
