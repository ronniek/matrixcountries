class TVSCollection {
    // members
    var sections: [TVSData]

    var count: Int {
        return sections.count
    }

    // class
    public init() {
        sections = [TVSData]()
    }
    
    // subscript
    public subscript(index: Int) -> TVSData {
        get {
            return sections[index]
        }
        set {
            sections[index] = newValue
        }
    }
}
