import UIKit

class UITableViewEx: UIView, UITableViewDelegate, UITableViewDataSource {
    // members
    let tableView = UITableView()
    var sections = TVSCollection()
    
    var buttonClickedFunction: ((ButtonTVCData) -> ())?
    var checkBoxClickedFunction: ((CheckBoxTVCData) -> ())?
    var segueFunction: ((LabelSegueTVCData) -> ())?
    var switchChangedFunction: ((SwitchTVCData) -> ())?
    var textFieldChangedFunction: ((TextFieldTVCData) -> ())?

    var isScrolledToBottom = false
    
    // class
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    init() {
        super.init(frame: CGRect.zero)
        
        backgroundColor = UIColor.init(red: 239, green: 239, blue: 244, alpha: 1)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.hideUnusedCells()
        self.addSubview(tableView)
        
        tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
    func setSections(_ sections: TVSCollection) {
        self.sections = sections
        tableView.reloadData()
  }
    func reloadData() {
        tableView.reloadData()
    }
    func reloadRows(_ indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.fade)
    }
    func addButtonClickedListener(_ buttonClickedFunction: @escaping (ButtonTVCData) -> ()) {
        self.buttonClickedFunction = buttonClickedFunction
    }
    func addCheckBoxClickedListener(_ checkBoxClickedFunction: @escaping (CheckBoxTVCData) -> ()) {
        self.checkBoxClickedFunction = checkBoxClickedFunction
    }
    func addSegueListener(_ segueFunction: @escaping (LabelSegueTVCData) -> ()) {
        self.segueFunction = segueFunction
    }
    func addSwitchChangedListener(_ switchChangedFunction: @escaping (SwitchTVCData) -> ()) {
        self.switchChangedFunction = switchChangedFunction
    }
    func addTextFieldChangedListener(_ textFieldChangedFunction: @escaping (TextFieldTVCData) -> ()) {
        self.textFieldChangedFunction = textFieldChangedFunction
    }

    // tableView
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: 30))
        label.text = sections[section].header
        return label
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 100, height: 30))
        label.text = sections[section].footer
        return label
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return sections[section].footer
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].rowHeight
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].header
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell = sections[indexPath.section].rows[indexPath.row]
        switch (tableViewCell.tableViewCellType) {
        case .Button:
            let buttonTVC = ButtonTVC(tableViewCell as! ButtonTVCData, buttonClicked)
            return buttonTVC
        case .TitleCheckBox:
            let checkBoxTVC = CheckBoxTVC(tableViewCell as! CheckBoxTVCData, checkBoxClicked)
            return checkBoxTVC
        case .TitleDetails:
            let detailsTVC = DetailsTVC(tableViewCell as! DetailsTVCData)
            return detailsTVC
        case .TitleLabel:
            let labelTVC = LabelTVC(tableViewCell as! LabelTVCData)
            return labelTVC
        case .TitleLabelSegue:
            let labelSegueTVC = LabelSegueTVC(tableViewCell as! LabelSegueTVCData, segueClicked)
            return labelSegueTVC
        case .TitleSwitch:
            let switchTVC = SwitchTVC(tableViewCell as! SwitchTVCData, switchChanged)
            return switchTVC
        case .TitleTextField:
            let textFieldTVC = TextFieldTVC(tableViewCell as! TextFieldTVCData, textFieldChanged)
            return textFieldTVC
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelect {0}-{1}".format(indexPath.section as AnyObject, indexPath.row as AnyObject))
        
        let tableViewCell = sections[indexPath.section].rows[indexPath.row]
        switch (tableViewCell.tableViewCellType) {
        case .Button:
            break;
        case .TitleCheckBox:
            if checkBoxClickedFunction != nil {
                checkBoxClickedFunction!(tableViewCell as! CheckBoxTVCData)
            }
            break;
        case .TitleDetails:
            break;
        case .TitleLabel:
            break;
        case .TitleLabelSegue:
            if (tableViewCell as! LabelSegueTVCData).performSegue != "" {
                let tvc = tableView.cellForRow(at: indexPath) as! LabelSegueTVC
                print(tvc.getValue)
            }
            break;
        case .TitleSwitch:
            break;
        case .TitleTextField:
            break;
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let tableViewCell = sections[indexPath.section].rows[indexPath.row]

        return tableViewCell.tableViewCellType == .TitleCheckBox
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        isScrolledToBottom = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
    }

    public func buttonClicked(_ buttonTVCData: ButtonTVCData) -> () {
        if buttonClickedFunction != nil {
            buttonClickedFunction!(buttonTVCData)
        }
    }
    public func checkBoxClicked(_ checkBoxTVCData: CheckBoxTVCData) -> () {
        if checkBoxClickedFunction != nil {
            checkBoxClickedFunction!(checkBoxTVCData)
        }
    }
    public func segueClicked(_ titleLabelSegueTVCData: LabelSegueTVCData) -> () {
        if segueFunction != nil {
            segueFunction!(titleLabelSegueTVCData)
        }
   }
    public func switchChanged(_ titleSwitchTVCData: SwitchTVCData) -> () {
        if switchChangedFunction != nil {
            switchChangedFunction!(titleSwitchTVCData)
        }
    }
    public func textFieldChanged(_ titleTextFieldTVCData: TextFieldTVCData) -> () {
        if textFieldChangedFunction != nil {
            textFieldChangedFunction!(titleTextFieldTVCData)
        }
    }
}
