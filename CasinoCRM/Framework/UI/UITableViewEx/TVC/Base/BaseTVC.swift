import UIKit;

class BaseTVC: UITableViewCell {
    // members
    var tableViewCellData: BaseTVCData
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ reuseIdentifier: String, _ tableViewCellData: BaseTVCData) {
        self.tableViewCellData = tableViewCellData
        
        super.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: reuseIdentifier)

        let tapGestureTVC = UITapGestureRecognizer(target: self, action: #selector(tvcTapped(sender:)))
        self.addGestureRecognizer(tapGestureTVC)
        tapGestureTVC.delegate = self
    }
    
    // methods
    func isEnabled(_ enabled: Bool) {
        isUserInteractionEnabled = enabled
        alpha = enabled ? 1 : 0.5
        backgroundColor = enabled ? UIColor.white : UIColor.lightGray
    }
    
    // events
    @objc func tvcTapped(sender: UITapGestureRecognizer) {
    }
}
