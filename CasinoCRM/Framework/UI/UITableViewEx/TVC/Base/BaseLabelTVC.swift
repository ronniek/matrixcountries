import UIKit;

class BaseLabelTVC: BaseTVC {
    // members
    var titleLabel: UILabel!

    var getTitle: String {
        return titleLabel.text ?? ""
    }

    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    override init(_ reuseIdentifier: String, _ tableViewCellData: BaseTVCData) {
        super.init(reuseIdentifier, tableViewCellData)
        
        titleLabel = UILabel()
        titleLabel.text = tableViewCellData.title
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.isUserInteractionEnabled = true
        contentView.addSubview(titleLabel)
        
        let tapGestureLabel = UITapGestureRecognizer(target: self, action: #selector(titleTapped(sender:)))
        titleLabel.addGestureRecognizer(tapGestureLabel)
        tapGestureLabel.delegate = self

        titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: 0).isActive = true
    }
    
    // events
    @objc func titleTapped(sender: UITapGestureRecognizer) {
    }
}
