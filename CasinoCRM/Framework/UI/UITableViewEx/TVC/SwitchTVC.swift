import UIKit;

class SwitchTVC: BaseLabelTVC {
    // members
    var onOffSwitch: UISwitch!
    var function: (SwitchTVCData) -> ()

    var getValue: Bool {
        return onOffSwitch.isOn
    }
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: SwitchTVCData, _ function: @escaping (SwitchTVCData) -> ()) {
        self.function = function
        super.init("SwitchTVC", tableViewCellData)

        onOffSwitch = UISwitch()
        onOffSwitch.isOn = tableViewCellData.isOn
        onOffSwitch.isEnabled = tableViewCellData.isOnEnabled
        onOffSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        
        onOffSwitch.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(onOffSwitch)
        
        titleLabel.trailingAnchor.constraint(equalTo: onOffSwitch.leadingAnchor).isActive = true

        onOffSwitch.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        onOffSwitch.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 0).isActive = true
    }
    
    // methods
    override func isEnabled(_ enabled: Bool) {
        super.isEnabled(enabled)
        onOffSwitch.isUserInteractionEnabled = false
        onOffSwitch.alpha = 0.5
    }
    
    // events
    @objc func switchChanged(switch: UISwitch) {
        print(getValue)
        (tableViewCellData as! SwitchTVCData).isOn = getValue
        function(tableViewCellData as! SwitchTVCData)
   }
}
