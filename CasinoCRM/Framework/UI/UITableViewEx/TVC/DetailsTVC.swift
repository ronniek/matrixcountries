import UIKit;

class DetailsTVC: BaseTVC {
    // members
    var titleLabel: UILabel!
    var valueLabel: UILabel!
    
    var getTitle: String {
        return titleLabel.text ?? ""
    }
    var getValue: String {
        return valueLabel.text ?? ""
    }
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: DetailsTVCData) {
        super.init("DetailsTVC", tableViewCellData)
        
        titleLabel = UILabel()
        titleLabel.text = tableViewCellData.title
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(titleLabel)

        valueLabel = UILabel()
        valueLabel.text = tableViewCellData.value
        valueLabel.textAlignment = .right
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(valueLabel)

        titleLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: 0).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 0).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor, constant: 0).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: valueLabel.topAnchor, constant: 0).isActive = true

        valueLabel.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: 0).isActive = true
        valueLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 0).isActive = true
        valueLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 0).isActive = true
    }
}
