import UIKit;

class TextFieldTVC: BaseLabelTVC {
    // members
    var textField: UITextField!
    var clearTextImageView: UIImageView!
    var function: (TextFieldTVCData) -> ()

    var getValue: String {
        return textField.text ?? ""
    }
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: TextFieldTVCData, _ function: @escaping (TextFieldTVCData) -> ()) {
        self.function = function
        super.init("TextFieldTVC", tableViewCellData)

        textField = UITextField()
        textField.text = tableViewCellData.value
        textField.font = UIFont(name: (textField.font?.fontName)!, size: 21)
        textField.layer.cornerRadius = 5
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.lightGray.cgColor
        
        textField.keyboardType = tableViewCellData.keyboardType
        
        textField.leftViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 4, height: 4))

        textField.rightViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 4))

        textField.addTarget(self, action: #selector(textFieldChanged), for: UIControl.Event.allEvents)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addKeyboardDoneButton()
        contentView.addSubview(textField)
        
        clearTextImageView = UIImageView()
        clearTextImageView.image = UIImage(named: "Clear")
        clearTextImageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(clearTextImageView)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(clearTextTapped(sender:)))
        clearTextImageView.addGestureRecognizer(tapGesture)
        clearTextImageView.isUserInteractionEnabled = true

        titleLabel.widthAnchor.constraint(equalToConstant: tableViewCellData.titleWidth).isActive = true

        textField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        textField.leadingAnchor.constraint(equalTo: titleLabel.layoutMarginsGuide.trailingAnchor, constant: 8).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textField.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 6).isActive = true

        clearTextImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        clearTextImageView.widthAnchor.constraint(equalToConstant: 26).isActive = true
        clearTextImageView.heightAnchor.constraint(equalToConstant: 26).isActive = true
        clearTextImageView.trailingAnchor.constraint(equalTo: textField.layoutMarginsGuide.trailingAnchor, constant: 6).isActive = true
    }
    
    // methods
    override func isEnabled(_ enabled: Bool) {
        super.isEnabled(enabled)
        textField.isUserInteractionEnabled = false
        textField.alpha = 0.5
        clearTextImageView.isUserInteractionEnabled = false
        clearTextImageView.alpha = 0.5
    }
    
    // events
    @objc override func tvcTapped(sender: UITapGestureRecognizer) {
        (tableViewCellData as! TextFieldTVCData).value = getValue
        function(tableViewCellData as! TextFieldTVCData)
    }
    @objc override func titleTapped(sender: UITapGestureRecognizer) {
        (tableViewCellData as! TextFieldTVCData).value = getValue
        function(tableViewCellData as! TextFieldTVCData)
    }
    @objc func textFieldChanged(textField: UITextField) {
        (tableViewCellData as! TextFieldTVCData).value = getValue
        function(tableViewCellData as! TextFieldTVCData)
    }
    @objc func clearTextTapped(sender: UITapGestureRecognizer) {
        textField.text = ""
        (tableViewCellData as! TextFieldTVCData).value = ""
        clearTextImageView.blink()
    }
}
