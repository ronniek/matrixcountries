import UIKit;

class LabelSegueTVC: BaseLabelTVC {
    // members
    var valueLabel: UILabel!
    var segueImageView: UIImageView!
    var function: (LabelSegueTVCData) -> ()

    var getValue: String {
        return valueLabel.text ?? ""
    }
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: LabelSegueTVCData, _ function: @escaping (LabelSegueTVCData) -> ()) {
        self.function = function
        super.init("LabelSegueTVC", tableViewCellData)
        
        valueLabel = UILabel()
//        valueLabel.textColor = tableViewCellData.valueColor
        valueLabel.text = tableViewCellData.value
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.isUserInteractionEnabled = true
        contentView.addSubview(valueLabel)
        
        let tapGestureLabel = UITapGestureRecognizer(target: self, action: #selector(labelTapped(sender:)))
        valueLabel.addGestureRecognizer(tapGestureLabel)
        tapGestureLabel.delegate = self

        segueImageView = UIImageView()
        segueImageView.image = UIImage(named: "Segue")
        segueImageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(segueImageView)
        
        let tapGestureSegue = UITapGestureRecognizer(target: self, action: #selector(segueTapped(sender:)))
        segueImageView.isUserInteractionEnabled = true
        segueImageView.addGestureRecognizer(tapGestureSegue)

        titleLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true

        valueLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        valueLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 8).isActive = true
        valueLabel.trailingAnchor.constraint(equalTo: segueImageView.leadingAnchor, constant: -8).isActive = true

        segueImageView.leadingAnchor.constraint(equalTo: valueLabel.trailingAnchor, constant: 8).isActive = true
        segueImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        segueImageView.widthAnchor.constraint(equalToConstant: 26).isActive = true
        segueImageView.heightAnchor.constraint(equalToConstant: 26).isActive = true
        segueImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
    }
    
    // methods
    override func isEnabled(_ enabled: Bool) {
        super.isEnabled(enabled)
        valueLabel.isUserInteractionEnabled = false
        valueLabel.alpha = 0.5
        segueImageView.isUserInteractionEnabled = false
        segueImageView.alpha = 0.5
    }
    
    // events
    @objc override func tvcTapped(sender: UITapGestureRecognizer) {
        function(tableViewCellData as! LabelSegueTVCData)
        segueImageView.blink()
    }
    @objc override func titleTapped(sender: UITapGestureRecognizer) {
        function(tableViewCellData as! LabelSegueTVCData)
        segueImageView.blink()
    }
    @objc func labelTapped(sender: UITapGestureRecognizer) {
        function(tableViewCellData as! LabelSegueTVCData)
        segueImageView.blink()
    }
    @objc func segueTapped(sender: UITapGestureRecognizer) {
        function(tableViewCellData as! LabelSegueTVCData)
        segueImageView.blink()
    }
}
