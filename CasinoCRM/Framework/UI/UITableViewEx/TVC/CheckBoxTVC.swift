import UIKit;

class CheckBoxTVC: BaseLabelTVC {
    // members
    var getValue: Bool {
        return isSelected
    }
    var function: (CheckBoxTVCData) -> ()

    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: CheckBoxTVCData, _ function: @escaping (CheckBoxTVCData) -> ()) {
        self.function = function
        super.init("CheckBoxTVC", tableViewCellData)
        
        titleLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 0).isActive = true

        if tableViewCellData.checkBox {
            accessoryType = UITableViewCell.AccessoryType.checkmark
            accessoryView = nil
        }
    }
    
    // events
    @objc override func titleTapped(sender: UITapGestureRecognizer) {
        accessoryType = UITableViewCell.AccessoryType.checkmark
        accessoryView = nil
        function(tableViewCellData as! CheckBoxTVCData)
    }
}
