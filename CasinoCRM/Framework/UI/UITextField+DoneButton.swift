import UIKit;

extension UITextField {
    func addKeyboardDoneButton() {
        if self.inputAccessoryView == nil {
            let toolbarDone = UIToolbar.init()
            toolbarDone.sizeToFit()
            let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(doneButtonAction))
            
            toolbarDone.items = [barBtnDone]
            self.inputAccessoryView = toolbarDone
        }
    }
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}
