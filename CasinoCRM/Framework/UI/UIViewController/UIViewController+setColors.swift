import Foundation
import UIKit

extension UIViewController {
    public func setColors() {
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "bg.jpg")!, for: .default)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg.jpg")!)
    }
}
