import ActionSheetPicker_3_0

public class ActionSheetPickerHelper {
    // SingleAnyPicker
    //    ActionSheetPickerHelper.SingleAnyPicker(logOutBarButton, "title", ["gmail.com", "yahoo.com", "microsoft.com", "hotmail.com", 1, 5.4], 2, done: { (actionSheetStringPicker, index, value) in
    //    	print("actionSheetStringPicker = \(actionSheetStringPicker)")
    //    	print("index = \(index)")
    //    	print("value = \(value)")
    //    }) { (actionSheetStringPicker) in
    //    	print("actionSheetStringPicker = \(actionSheetStringPicker)")
    //    }
    public static func SingleAnyPicker(_ origin: Any?, _ title: String, _ rows: [Any]?, _ defaultIndex: Int, _ done: @escaping (ActionSheetStringPicker?, Int, Any?) -> Void, _ cancel: @escaping (ActionSheetStringPicker?) -> Void) {
        ActionSheetStringPicker.show(withTitle: title, rows: rows, initialSelection: defaultIndex, doneBlock: done, cancel: cancel, origin: origin)
    }
}
