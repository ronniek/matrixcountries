import Foundation
import UIKit

open class BaseUITableViewController: UITableViewController {
    // members
    public var isVisible1: Bool = false {
        didSet {
            if isVisible1 {
                attachTimeoutListener()
            } else {
                detachTimeoutListener()
            }
        }
    }
    private var recentInteraction: Date?
    
    public var navigationLeftButtonsEnabled: Bool {
        didSet {
            self.navigationItem.setHidesBackButton(!navigationLeftButtonsEnabled, animated:false);
            
            if self.navigationItem.leftBarButtonItems != nil {
                for item in self.navigationItem.leftBarButtonItems! {
                    item.isEnabled = navigationLeftButtonsEnabled
                }
            }
        }
    }
    public var navigationRightButtonsEnabled: Bool {
        didSet {
            if self.navigationItem.rightBarButtonItems != nil {
                for item in self.navigationItem.rightBarButtonItems! {
                    item.isEnabled = navigationRightButtonsEnabled
                }
            }
        }
    }
    
    // class
    public required init?(coder aDecoder: NSCoder) {
        navigationLeftButtonsEnabled = true
        navigationRightButtonsEnabled = true
        
        super.init(coder: aDecoder)
    }
    deinit {
        ApplicationHandler.instance.appDelegate.removeApplicationDidBecomeActiveEventListener()
        ApplicationHandler.instance.appDelegate.removeApplicationDidEnterBackgroundEventListener()
    }
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        ApplicationHandler.instance.appDelegate.addApplicationDidBecomeActiveEventListener { _ in
            self.attachTimeoutListener()
        }
        ApplicationHandler.instance.appDelegate.addApplicationDidEnterBackgroundEventListener { _ in
            self.detachTimeoutListener()
        }
        
        closeWhenTapped()
    }
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        isVisible1 = true
    }
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isVisible1 = false
    }
    
    // timeout
    open func checkInteractionTimeout() -> Bool {
        return false
    }
    private func attachTimeoutListener() {
        if checkInteractionTimeout() {
            if recentInteraction == nil {
                recentInteraction = Date()
            }
            
            ApplicationHandler.instance.timer.addTickEventListener(10) { _ in
                if (self.recentInteraction?.timeIntervalSince1970)! + Consts.interactionTimeout < Date().timeIntervalSince1970 {
                    ApplicationHandler.instance.timer.removeTickEventListener(10)
                    self.onTimeout()
                }
            }
        }
    }
    private func detachTimeoutListener() {
        ApplicationHandler.instance.timer.removeTickEventListener(10)
        recentInteraction = nil
    }
    
    public func interaction() {
        recentInteraction = Date()
    }
    public func onTimeout() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC")
        self.navigationController?.pushViewController(loginVC, animated: true)
        PopupsHelper.presentDismissMessageBox(self, "Security Message", "You are logged out because you have not been active, Please login again", "Close")
    }
}
