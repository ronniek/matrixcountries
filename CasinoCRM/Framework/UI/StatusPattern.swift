import UIKit

class StatusPattern: BaseUIViewController {
    // members
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    public override func checkInteractionTimeout() -> Bool {
        return true
    }
    
    // status
    enum StatusEnum {
        case disabled
        case enabled
    }
    private var status: StatusEnum {
        didSet {
            switch status {
            case .disabled:
                navigationLeftButtonsEnabled = false
                navigationRightButtonsEnabled = false
                address1TextField.isEnabled = false
                address2TextField.isEnabled = false
                cityTextField.isEnabled = false
            case .enabled:
                navigationLeftButtonsEnabled = true
                navigationRightButtonsEnabled = true
                address1TextField.isEnabled = true
                address2TextField.isEnabled = true
                cityTextField.isEnabled = true
            }
        }
    }
    
    // class
    required init?(coder aDecoder: NSCoder) {
        status = .disabled
        
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        address1TextField.text = "initial value"
        address2TextField.text = "initial value"
        cityTextField.text = "initial value"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        status = .enabled
    }
    
    // events
    @IBAction func saveButton(_ sender: Any) {
        status = .disabled

        // do something
        // status = .enabled
    }
    @IBAction func address1ValueChanged(_ sender: Any) {
        interaction()
    }
}
