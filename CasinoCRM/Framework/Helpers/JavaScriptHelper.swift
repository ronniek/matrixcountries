import Foundation
import JavaScriptCore

public class JavaScriptHelper {
    // evaluate a javascript expression
    public static func execute(_ javascript: String) -> String {
        let jsContext = JSContext()
        return (jsContext?.evaluateScript(javascript))!.toString()
    }
    
    // evaluate a javascript expression
    // in: expression with %0, %1, %n parameters replaced by params
    // sample:
    //  execute("%0 + %1 > %2", "3", "4", "6") -> "true"
    public static func execute(_ javascript: String, _ params: String...) -> String {
        var newJavascript = javascript
        for (index, param) in params.enumerated() {
            newJavascript = newJavascript.replacingOccurrences(of: "%\(index)", with: param)
        }

        return execute(newJavascript)
    }
}
