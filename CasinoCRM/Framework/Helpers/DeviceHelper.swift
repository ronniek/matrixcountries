import Foundation
import AudioToolbox
import AVFoundation
import UIKit

public class DeviceHelper {
    //    UIDevice.current.batteryLevel
    //    UIDevice.current.batteryState
    //    UIDevice.current.identifierForVendor
    //    UIDevice.current.isBatteryMonitoringEnabled
    //    UIDevice.current.isProximityMonitoringEnabled
    //    UIDevice.current.localizedModel
    //    UIDevice.current.model
    //    UIDevice.current.name
    //    UIDevice.current.orientation
    //    UIDevice.current.proximityState
    //    UIDevice.current.systemName
    //    UIDevice.current.systemVersion
    //    UIDevice.current.userInterfaceIdiom
    
    public static func getName() -> String {
        return UIDevice.current.name;
    }
    
    public static func vibrate() {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }
    public static func beep() {
        // https://github.com/TUNER88/iOSSystemSoundsLibrary
        let systemSoundID: SystemSoundID = 1106
        AudioServicesPlaySystemSound (systemSoundID)
    }
    
    public static func isSimulator() -> Bool {
        #if targetEnvironment(simulator)
            return true
        #else
            return false
        #endif
    }
}
