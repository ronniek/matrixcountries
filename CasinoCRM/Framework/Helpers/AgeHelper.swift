import Foundation

public class AgeHelper {
    public static func Age(_ dt: Date) -> String {
        var result: String = ""

        let now = Date();
        
        var years = now.year - dt.year;
        var months = now.month - dt.month;
        var days = now.day - dt.day;
        
        if (days < 0) {
            months = months - 1;
            days = 0;
        }
        if (months < 0) {
            years = years - 1;
            months = 11;
        }
        if (years < 0) {
            years = 0;
        }
        
        if (years == 1) {
            result += "1 Year";
        } else if (years > 1) {
            result = result + "\(years) Years";
        }
        if (months == 1) {
            if (result != "") {
                result += " ";
            }
            result += "1 Month";
        } else if (months > 1) {
            if (result != "") {
                result += " ";
            }
            result = result + "\(months) Months";
        }

        return result
    }
}
