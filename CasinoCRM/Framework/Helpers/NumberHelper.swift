import Foundation

public class NumberHelper {
    // decimal formatter
    // "-$54,234,324.23"
    public static func numberCurrencyFormat(_ number: Decimal) -> String {
        return numberFormat(number, .currency)
    }
    // "($54,234,324.23)"
    public static func numberAccountingFormat(_ number: Decimal) -> String {
        return numberFormat(number, .currencyAccounting)
    }
    // "-USD54,234,324.23"
    public static func numberISOFormat(_ number: Decimal) -> String {
        return numberFormat(number, .currencyISOCode)
    }
    // "-54,234,324.23 US dollars"
    public static func numberPluralFormat(_ number: Decimal) -> String {
        return numberFormat(number, .currencyPlural)
    }
    // "-54,234,324.234"
    public static func numberDecimalFormat(_ number: Decimal) -> String {
        return numberFormat(number, .decimal)
    }
    // "-54234324"
    public static func numberNoneFormat(_ number: Decimal) -> String {
        return numberFormat(number, .none)
    }
    // "−54,234,324th"
    public static func numberOrdinalFormat(_ number: Decimal) -> String {
        return numberFormat(number, .ordinal)
    }
    // "minus fifty-four million two hundred thirty-four thousand three hundred twenty-four point two three four"
    public static func numberSpellOutFormat(_ number: Decimal) -> String {
        return numberFormat(number, .spellOut)
    }
    private static func numberFormat(_ number: Decimal, _ style: NumberFormatter.Style) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = style
        return numberFormatter.string(for: number) ?? ""
    }
    
    // double formatter
    // "-$54,234,324.23"
    public static func numberCurrencyFormat(_ number: Double) -> String {
        return numberFormat(number, .currency)
    }
    // "($54,234,324.23)"
    public static func numberAccountingFormat(_ number: Double) -> String {
        return numberFormat(number, .currencyAccounting)
    }
    // "-USD54,234,324.23"
    public static func numberISOFormat(_ number: Double) -> String {
        return numberFormat(number, .currencyISOCode)
    }
    // "-54,234,324.23 US dollars"
    public static func numberPluralFormat(_ number: Double) -> String {
        return numberFormat(number, .currencyPlural)
    }
    // "-54,234,324.234"
    public static func numberDecimalFormat(_ number: Double) -> String {
        return numberFormat(number, .decimal)
    }
    // "-54234324"
    public static func numberNoneFormat(_ number: Double) -> String {
        return numberFormat(number, .none)
    }
    // "−54,234,324th"
    public static func numberOrdinalFormat(_ number: Double) -> String {
        return numberFormat(number, .ordinal)
    }
    // "minus fifty-four million two hundred thirty-four thousand three hundred twenty-four point two three four"
    public static func numberSpellOutFormat(_ number: Double) -> String {
        return numberFormat(number, .spellOut)
    }
    private static func numberFormat(_ number: Double, _ style: NumberFormatter.Style) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = style
        return numberFormatter.string(for: number) ?? ""
    }
    
    // float formatter
    // "-$54,234,324.23"
    public static func numberCurrencyFormat(_ number: Float) -> String {
        return numberFormat(number, .currency)
    }
    // "($54,234,324.23)"
    public static func numberAccountingFormat(_ number: Float) -> String {
        return numberFormat(number, .currencyAccounting)
    }
    // "-USD54,234,324.23"
    public static func numberISOFormat(_ number: Float) -> String {
        return numberFormat(number, .currencyISOCode)
    }
    // "-54,234,324.23 US dollars"
    public static func numberPluralFormat(_ number: Float) -> String {
        return numberFormat(number, .currencyPlural)
    }
    // "-54,234,324.234"
    public static func numberDecimalFormat(_ number: Float) -> String {
        return numberFormat(number, .decimal)
    }
    // "-54234324"
    public static func numberNoneFormat(_ number: Float) -> String {
        return numberFormat(number, .none)
    }
    // "−54,234,324th"
    public static func numberOrdinalFormat(_ number: Float) -> String {
        return numberFormat(number, .ordinal)
    }
    // "minus fifty-four million two hundred thirty-four thousand three hundred twenty-four point two three four"
    public static func numberSpellOutFormat(_ number: Float) -> String {
        return numberFormat(number, .spellOut)
    }
    private static func numberFormat(_ number: Float, _ style: NumberFormatter.Style) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = style
        return numberFormatter.string(for: number) ?? ""
    }
}
