import Foundation
import CommonCrypto

public extension String {
    // extentions
    public func count() -> Int {
        return self.count
    }
    
    // format
    public func format(_ args: AnyObject...) -> String {
        var result = self
        for (index, arg) in args.enumerated() {
            result = result.replacingOccurrences(of: "{\(index)}", with: String(describing: arg))
        }
        return result
    }
    
    // substring
    public func substring(_ from: Int, _ length: Int) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: from)
        let endIndex = self.index(self.startIndex, offsetBy: from + length - 1)
        return String(self[startIndex...endIndex])
    }
    public func left(_ left: Int) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: 0)
        let endIndex = self.index(self.startIndex, offsetBy: 0 + left - 1)
        return String(self[startIndex...endIndex])
    }
    public func right(_ right: Int) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: self.count - right)
        let endIndex = self.index(self.startIndex, offsetBy: self.count - 1)
        return String(self[startIndex...endIndex])
    }

    // sub set
    public func getAlphaSubSet() -> String {
        return getSubSet(Consts.alpha)
    }
    public func getALPHASubSet() -> String {
        return getSubSet(Consts.ALPHA)
    }
    public func getNumericSubSet() -> String {
        return getSubSet(Consts.numeric)
    }
    public func getAlphaNumericSubSet() -> String {
        return getSubSet(Consts.alphaNumeric)
    }
    private func getSubSet(_ contains: String) -> String {
        return self.components(separatedBy: CharacterSet.init(charactersIn: contains).inverted).joined(separator: "")
    }
    
    // contains only
    public func containsAlphaOnly() -> Bool {
        return containsOnly(Consts.alpha)
    }
    public func containsALPHAOnly() -> Bool {
        return containsOnly(Consts.ALPHA)
    }
    public func containsNumericOnly() -> Bool {
        return containsOnly(Consts.numeric)
    }
    public func containsAlphaNumericOnly() -> Bool {
        return containsOnly(Consts.alphaNumeric)
    }
    private func containsOnly(_ contains: String) -> Bool {
        return self.count() == getSubSet(contains).count()
    }
    
    // special
    public func removeAllSpaces() -> String {
        return self.replacingOccurrences(of: " ", with: "")
    }
    
    public func concat(_ sep: String, _ str: String) -> String {
        if self == ""  && str == "" {
            return ""
        } else if self == "" {
            return str
        } else if str == "" {
            return self
        } else {
            return self + sep + str
        }
    }
    
    // index
    public func indexOf(_ character: Character) -> Int {
        if let index = self.index(of: character) {
            let position = self.distance(from: self.startIndex, to: index)
            return position
        } else {
            return -1
        }
    }

    // contain
    public func contains(_ character: Character) -> Bool {
        return self.indexOf(character) >= 0
    }

    public func SHA256() -> String {
        let str = "\(self)1f5a98e07fbf44919705e5ddd3fa8aeb"
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), str, str.count, self, self.count, &digest)
        let data = Data(bytes: digest)
        return data.map { String(format: "%02hhx", $0) }.joined()
    }
}
