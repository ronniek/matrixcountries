import Foundation

public class DateTimeHelper {
    // "Wednesday, March 8, 2017"
    public static func dateFullFormat(_ date: Date) -> String {
        return dateFormat(date, .full)
    }
    // "March 8, 2017"
    public static func dateLongFormat(_ date: Date) -> String {
        return dateFormat(date, .long)
    }
    // "Mar 8, 2017"
    public static func dateMediumFormat(_ date: Date) -> String {
        return dateFormat(date, .medium)
    }
    // "3/8/17"
    public static func dateShortFormat(_ date: Date) -> String {
        return dateFormat(date, .short)
    }
    private static func dateFormat(_ date: Date, _ style: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = style
        return dateFormatter.string(from: date)
    }
    
    public static func dateFormat(_ date: Date, _ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }

    // string 2 date
    public static func getDate_yyyyMMdd(_ string: String) -> Date? {
        return getDate(string, "yyyyMMdd")
    }
    public static func getDate_MMMddyyyy(_ string: String) -> Date? {
        return getDate(string, "MMM dd yyyy")
    }
    private static func getDate(_ string: String, _ format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: string)
    }
    
    // UTC
    // "2017-03-08T13:05:10-0500"
    public static func dateToUTC(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Consts.UTC_DATE_TIME_FORMAT
        return dateFormatter.string(from: date)
    }
    public static func dateFromUTC(_ utc: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Consts.UTC_DATE_TIME_FORMAT
        return dateFormatter.date(from: utc)
    }

    // YYYYMMDD_HHMMSS
    // "2017-03-14 13:05:10"
    public static func dateToYYYYMMDD_HHMMSS(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Consts.YYYYMMDD_HHMMSS_FORMAT
        return dateFormatter.string(from: date)
    }
    public static func dateFromYYYYMMDD_HHMMSS(_ utc: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Consts.YYYYMMDD_HHMMSS_FORMAT
        return dateFormatter.date(from: utc)
    }
}
