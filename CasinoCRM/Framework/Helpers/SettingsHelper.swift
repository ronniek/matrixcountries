import Foundation

public class SettingsHelper {
    private static let STYLE_TYPE = "STYLE_TYPE"
    
    public static func getStyleType() -> StyleTypeEnum {
        var result: StyleTypeEnum

        let styleType = SettingsHelper.string(SettingsHelper.STYLE_TYPE)
        switch styleType {
        case "1":
            result = StyleTypeEnum.light
        case "2":
            result = StyleTypeEnum.dark
        case "3":
            result = StyleTypeEnum.color
        default:
            setStyleType(StyleTypeEnum.light)
            result = StyleTypeEnum.light
        }

        return result
    }
    public static func setStyleType(_ styleType: StyleTypeEnum) {
        SettingsHelper.set(STYLE_TYPE, styleType.rawValue)
    }
    
    public static func string(_ key: String) -> String {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }
    public static func int(_ key: String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }
    public static func decimal(_ key: String) -> Decimal {
        return Decimal(UserDefaults.standard.double(forKey: key))
    }
    public static func bool(_ key: String) -> Bool {
        return UserDefaults.standard.bool(forKey: key)
    }
    public static func arrayDic(_ key: String) -> Array<[String:Any]> {
        return UserDefaults.standard.array(forKey: key) as? Array<[String:Any]> ?? Array<[String:Any]>()
    }
    public static func arrayString(_ key: String) -> Array<String> {
        return UserDefaults.standard.array(forKey: key) as? Array<String> ?? Array<String>()
    }

    public static func set(_ key: String, _ value: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    public static func set(_ key: String, _ value: Int) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    public static func set(_ key: String, _ value: Decimal) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    public static func set(_ key: String, _ value: Bool) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    public static func set(_ key: String, _ value: Array<[String:Any]>) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    public static func set(_ key: String, _ value: Array<String>) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    public static func remove(_ key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
}
