import Foundation
import AVFoundation
import UIKit

@UIApplicationMain
open class AppDelegateEx: UIResponder, UIApplicationDelegate {
    // members
    open var window: UIWindow?
    
    // class
    open func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print(FileHelper.getApplicationDirectory().path as Any)

        _ = ApplicationHandler.instance
        //_ = LocalizationManager.instance
        //_ = ResourceManager.instance
                
        ApplicationHandler.instance.appDelegate.RaiseApplicationDidFinishLaunchingWithOptionsEvent()
        
        return true
    }
 
    // lifecycle
    open func applicationWillResignActive(_ application: UIApplication) {
        ApplicationHandler.instance.appDelegate.RaiseApplicationWillResignActiveEvent()
    }
    open func applicationDidEnterBackground(_ application: UIApplication) {
        ApplicationHandler.instance.appDelegate.RaiseApplicationDidEnterBackgroundEvent()
    }
    open func applicationWillEnterForeground(_ application: UIApplication) {
        ApplicationHandler.instance.appDelegate.RaiseApplicationWillEnterForegroundEvent()
    }
    open func applicationDidBecomeActive(_ application: UIApplication) {
        ApplicationHandler.instance.appDelegate.RaiseApplicationDidBecomeActiveEvent()
    }
    open func applicationWillTerminate(_ application: UIApplication) {
        ApplicationHandler.instance.appDelegate.RaiseApplicationWillTerminateEvent()
    }
    
    func resetApp() {
        UIApplication.shared.windows[0].rootViewController = UIStoryboard(
            name: "Main",
            bundle: nil
            ).instantiateInitialViewController()
    }
}
