import Foundation
import Alamofire

public class NetworkStatusHandler: EventDispatcher {
    // members
    private var networkReachabilityManager = NetworkReachabilityManager(host: "www.google.com")
    
    public var isReachable: Bool {
        return networkReachabilityManager!.isReachable
    }

    // event dispatcher
    private let NetworkStatusChanged = "NetworkStatusChanged"
    public func addNetworkStatusChangedEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(NetworkStatusChanged, function)
    }

    // class
    override init() {
        super.init()
        start()
    }
    
    // methods
    public func start() {
        networkReachabilityManager?.listener = { status in
            self.RaiseEvent(self.NetworkStatusChanged, self)
        }
        
        networkReachabilityManager?.startListening()
    }
    public func stop() {
        networkReachabilityManager?.stopListening()
    }
}
