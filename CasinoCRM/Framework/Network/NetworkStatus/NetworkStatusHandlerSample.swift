import Foundation

public class NetworkStatusHandlerSample {
    public func doSome() {
        ApplicationHandler.instance.networkStatus.addNetworkStatusChangedEventListener(NetworkChanged)
    }
    
    public func NetworkChanged(_ eventResult: EventResult) -> () {
        let networkStatus = eventResult.sender as! NetworkStatusHandler
        print("NetworkChanged(\(String(describing: eventResult.sender)), \(eventResult.type), \(networkStatus.isReachable))")
    }
}
