import Foundation
import ObjectMapper

open class BaseCollection<T: BaseData>: EventDispatcher {
    // members
    private var maxID: Int
    public var collection: [T]
    
    // class
    public override init() {
        maxID = -1
        collection = [T]()
        super.init()
    }
    
    // subscript
    public subscript(index: Int) -> T {
        get {
            return collection[index]
        }
        set {
            collection[index] = newValue
        }
    }
    
    // event dispatcher
    public let CollectionChangedEvent = "CollectionChangedEvent"
    public func addCollectionChangedEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(CollectionChangedEvent, function)
    }
    public func removeCollectionChangedEventListener() {
        removeEventListener(CollectionChangedEvent)
    }
    
    // methods
    private func getNewID() -> Int {
        if maxID == -1 {
            maxID = 0
        } else {
            maxID = maxID + 1
        }
        return maxID
    }
    
    func idExists(_ id: Int) -> Bool {
        return collection.contains(where: {$0.id == id})
    }
    
    // crud
    public func add(_ item: T) {
        item.id = getNewID()
        collection.append(item)
        RaiseEvent(CollectionChangedEvent, self)
    }
    
    public func deleteID(_ id: Int) {
        let index = collection.index(where: {$0.id == id})
        if index != nil {
            collection.remove(at: index!)
        }
    }
}
