import Foundation
import ObjectMapper

open class BaseData: EventDispatcher {
    // members
    public var id: Int
    
    // class
    public override init() {
        id = 0
        super.init()
    }
    
    // event dispatcher
    public let DataChangedEvent = "DataChangedEvent"
    public func addDataChangedEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(DataChangedEvent, function)
    }
}
