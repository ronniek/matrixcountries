import Foundation
import ObjectMapper

open class BaseSerialzableData: BaseMappableDispatcher {
    // members
    var directory: URL
    var path: URL
    
    var timeStamp: Date

    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        timeStamp <- (map["timeStamp"], DateTransform())
    }

    // class
    public required init?(map: Map) {
        directory = FileHelper.getLibraryDirectory("Data")
        FileHelper.createDirectory(directory)

        path = FileHelper.concat(directory, "\(type(of: self)).json")!
        
        timeStamp = Date()

        super.init(map: map)
    }
    
    // methods
    public func setOwner(_ owner: String) {
        path = FileHelper.concat(directory, "\(owner)_\(type(of: self)).json")!
	}
    public func save() {
        DataLayerHelper.writeJsonToPath(self.toJSONString()!, path)
    }
    public func loadJson() -> String {
        return DataLayerHelper.readJsonFromPath(path)
    }
    
    public func exists() -> Bool {
        return FileHelper.exists(path)
    }
    
    // time stamp
    public func isNewer(_ date: Date) -> Bool {
        return date > timeStamp
    }
}
