import Foundation
import ObjectMapper

open class BaseMappableDispatcher: EventDispatcher, Mappable {
    // members
    
    // mapping
    open func mapping(map: Map) {
    }
    
    // class
    public required init?(map: Map) {
        super.init()
    }
    public override init() {
        super.init()
    }
}
