import Foundation

open class ApplicationHandler: EventDispatcher {
    // singleton
    public static let instance = ApplicationHandler()

    // members
    public var appDelegate = AppDelegateHandler()
    public var networkStatus = NetworkStatusHandler()
    public var timer = TimerHandler()
}
