Hi,

Thanks for letting me take coding challenge

My recent Swift project includes some framework code I created to tackle some of the common coding challenges including the ones you have in this coding challenge
I developed this project my self so there are little to no comments in it

I took my code and removed my the project specific implementation and implemented your requirement using my framework

The specific implementation is in
Access to API
    CasinoCRM/Network/WS/WSAdapter/CountryAdapter.swift
Data
    CasinoCRM/Network/WSData/CountryWSData.swift
UI
    CasinoCRM/UI/CountriesVC.swift
    CasinoCRM/UI/CountriesTableViewCell.swift
    CasinoCRM/UI/CountryVC.swift


Since I used a UITableViewController than there is no separation to MVC or MVVM in this sample.

The first screen implements a standard TableViewController with the delegate in it

The second screen is implemented using my own design of TableView implementation
You can see that there is no code that deals with the TableView but rather just declaring what the table view need to present and the implementation is done by the framework I built
(This is a sample of what my code really looks like)

There is also the implementation of data fetching from the URL, this is done with 2 components, the DataAdapter that extends AlamoFire and WSData which hold the data and enables to parse the JSON from the DataAdapter into a domain specific class

I did not pay attention to any design aspects like color and font

I'd be happy to come to the office and explain the framework in more details


Regards,
Ronnie
